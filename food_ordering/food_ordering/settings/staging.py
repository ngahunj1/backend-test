from .base import *  # noqa

DEBUG = int(config("DEBUG", default=0))  # noqa


ALLOWED_HOSTS = ["*"]
