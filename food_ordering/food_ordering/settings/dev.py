from .base import *  # noqa

DEBUG = int(config("DEBUG", default=1))  # noqa


ALLOWED_HOSTS = ["*"]
