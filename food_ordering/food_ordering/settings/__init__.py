from decouple import config

ENV = config("ENV", default="DEV")

if ENV == "DEV":
    from .dev import *  # noqa

elif ENV == "PROD":
    from .prod import *  # noqa

elif ENV == "STAGING":
    from .staging import *  # noqa

else:
    from .dev import *  # noqa
