import logging
from django.utils.translation import gettext_lazy as _
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView, ListCreateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from .permissions import IsChef, IsMananger, IsUser

from apps.orders.models import Order
from apps.catalog.models import Menu, Product

from .serializers import (
    OrderSerializer,
    OrderStatusSerializer,
    MenuSerializer,
    ProductSerializer,
    PlaceOrderSerializer,
)

logger = logging.getLogger(__name__)


class OrderListView(ListAPIView):
    permission_classes = [IsAuthenticated, IsMananger | IsChef]
    queryset = Order.objects.all()
    serializer_class = OrderSerializer


class TrackOrderView(APIView):
    """
    Description: Track an order.\n
    Accepts headers: {Authorization: Token 9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b}\n
    Type of request: GET\n
    Request data type: JSON\n

    Sample GET request:\n
    {
        "order_number": 260511
    }

    Response success status: HTTP_200_OK \n
    Sample Response success:\n
    {
        "number": "260511",
        "status": "In Progress"
    }

    """

    permission_classes = [IsAuthenticated, IsUser]
    serializer_class = OrderStatusSerializer

    def get(self, request, format=None):
        if "order_number" not in request.data:
            return Response(
                {"error": "Order number not provided"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        order = self.get_order()
        if order:
            serializer = OrderStatusSerializer(order)
            return Response(serializer.data, status=status.HTTP_200_OK)

        else:
            return Response(
                {"error": "Order not found"}, status=status.HTTP_404_NOT_FOUND
            )

    def get_order(self):
        order = None
        order_number = self.request.data.get("order_number")
        try:
            order = Order.objects.get(number=order_number)

        except Exception as e:
            logger.warning(f"get_order error {e}")

        return order


class UpdateOrderStatusView(APIView):
    """
    Description: Update an order status.\n
    Accepts headers: {Authorization: Token 9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b}\n
    Type of request: POST/GET\n
    Request data type: JSON\n

    Sample GET successful request:\n
    {
        "order_number": 260511,
        "status": "Pending"
    }

    Response success status: HTTP_200_OK \n
    Sample Response success:\n
    {
        "number": "260511",
        "status": "Pending"
    }
    """

    permission_classes = [IsAuthenticated, IsMananger | IsChef]
    serializer_class = OrderStatusSerializer

    def get(self, request, format=None):
        if "order_number" not in request.data:
            return Response(
                {"error": "Order number and new status must provided"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        if "status" not in request.data:
            return Response(
                {"error": "Order number and new status must provided"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        new_status = self.request.data.get("status")

        if new_status not in Order.OrderStatusPipeline.values:
            return Response(
                {
                    "error": f"Invalid order status. Allowed statuses are {Order.OrderStatusPipeline.values}"
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

        order = self.get_order()
        if order:
            if new_status not in order.get_available_statuses():
                available_statuses = order.get_available_statuses()
                err_msg = (
                    "Invalid order status. Order status cannot be updated."
                    if not available_statuses
                    else f"Invalid order status. Allowed statuses are {order.get_available_statuses()}"
                )
                return Response(
                    {"error": err_msg},
                    status=status.HTTP_400_BAD_REQUEST,
                )

            if (
                new_status == Order.OrderStatusPipeline.COMPLETED
                and not request.user.is_chef
            ):
                return Response(
                    {"error": "You do not have permission to perform this action"},
                    status=status.HTTP_403_FORBIDDEN,
                )

            order.set_status(new_status)
            serializer = OrderStatusSerializer(order)
            return Response(serializer.data, status=status.HTTP_200_OK)

        else:
            return Response(
                {"error": "Order not found"}, status=status.HTTP_404_NOT_FOUND
            )

    def get_order(self):
        order = None
        order_number = self.request.data.get("order_number")
        try:
            order = Order.objects.get(number=order_number)

        except Exception as e:
            logger.warning(f"get_order error {e}")

        return order


class PlaceOrderView(APIView):
    """
    Description: Place an order.\n
    Accepts headers: {Authorization: Token 9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b}\n
    Type of request: POST/GET\n
    Request data type: JSON\n

    Sample POST Request success:\n
    {
        "items": [
            {"product": 3,"quantity": 4},
            {"product": 4,"quantity": 4},
            {"product": 1,"quantity": 40},
            {"product": 5,"quantity": 94}
        ]
    }
    Response success status: HTTP_201_Created \n
    {
        "success": true,
        "message": "Order placed successfully"
    }


    """

    permission_classes = [IsAuthenticated, IsUser]
    serializer_class = PlaceOrderSerializer

    def post(self, request, format=None):
        serializer = PlaceOrderSerializer(data=request.data)

        if serializer.is_valid(raise_exception=True):
            serializer.save()
            context = {"success": True, "message": "Order placed successfully"}

            return Response(context, status=status.HTTP_201_CREATED)
        return Response({}, status=status.HTTP_400_BAD_REQUEST)


class MenusView(ListCreateAPIView):
    permission_classes = [IsMananger]
    queryset = Menu.objects.all()
    serializer_class = MenuSerializer


class ProductsView(ListCreateAPIView):
    """
    Description: List and create products.\n
    Accepts headers: {Authorization: Token 9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b}\n
    Type of request: POST/GET\n
    Request data type: JSON\n

    Response success status: HTTP_200_OK \n
    Sample GET Response success:\n
    [
        {
            "name": "Burger",
            "description": "Burger",
            "price": "900.00",
            "image": "http://127.0.0.1:6070/media/products-images/social01.jpeg"
        },
    {
        "name": "Pizza",
        "description": "Pizza",
        "price": "1999.00",
        "image": "http://127.0.0.1:6070/media/products-images/blog-03.jpg"
    },
    {
        "name": "Rice",
        "description": "Rice",
        "price": "2300.00",
        "image": "http://127.0.0.1:6070/media/products-images/TED_5718.webp"
    }
    ]

    Sample POST Request:\n
        {
            "name": "Cloves",
            "description": "Cloves",
            "price": "500.00",
            "image": "path to image"
        }
    Response success status: HTTP_201_Created \n
        {
            "name": "Cloves",
            "description": "Cloves",
            "price": "500.00",
            "image": "path to image"
        }
    """

    permission_classes = [IsAuthenticated, IsMananger]
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class RootAPIView(APIView):
    """List all api endpoints"""

    permission_classes = [IsAuthenticated]

    def get(self, request):
        from rest_framework.reverse import reverse

        urls = {
            "orders": reverse("api:order-list", request=request),
            "update-order-status": reverse("api:order-update-status", request=request),
            "track-order": reverse("api:track-order", request=request),
            "menus": reverse("api:menus", request=request),
            "products": reverse("api:products", request=request),
            "place-order": reverse("api:place-order", request=request),
        }
        return Response(urls)
