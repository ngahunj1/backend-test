from rest_framework.permissions import BasePermission


class IsChef(BasePermission):
    def has_permission(self, request, view):
        return request.user.is_chef


class IsMananger(BasePermission):
    def has_permission(self, request, view):
        return request.user.is_manager


class IsUser(BasePermission):
    def has_permission(self, request, view):
        return request.user.is_user
