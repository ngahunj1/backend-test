from django.urls import path
from .views import (
    OrderListView,
    TrackOrderView,
    MenusView,
    ProductsView,
    UpdateOrderStatusView,
    RootAPIView,
    PlaceOrderView,
)

app_name = "api"

urlpatterns = [
    path("orders/", OrderListView.as_view(), name="order-list"),
    path("orders/place/", PlaceOrderView.as_view(), name="place-order"),
    path(
        "orders/update-status/",
        UpdateOrderStatusView.as_view(),
        name="order-update-status",
    ),
    path("track-order/", TrackOrderView.as_view(), name="track-order"),
    path("menu/", MenusView.as_view(), name="menus"),
    path("products/", ProductsView.as_view(), name="products"),
    path("", RootAPIView.as_view(), name="root"),
]
