from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework import status
from apps.orders.models import Order, OrderLine
from apps.catalog.models import Product, Menu
from rest_framework.authtoken.models import Token

from django.contrib.auth import get_user_model

User = get_user_model()


class OrderListViewTests(APITestCase):
    def setUp(self):
        self.manager_user = User.objects.create_user(
            email="manager@test.com",
            password="testpassword",
            user_role=User.UserRolesChoices.RESTAURANT_MANAGER,
        )
        self.chef_user = User.objects.create_user(
            email="chef@test.com",
            password="testpassword",
            user_role=User.UserRolesChoices.CHEF,
        )
        self.manager_token, _ = Token.objects.get_or_create(user=self.manager_user)
        self.chef_token, _ = Token.objects.get_or_create(user=self.chef_user)
        self.order = Order.objects.create(
            number="123", total=100.0, status="Pending", payment_status="Not Paid"
        )
        self.url = reverse("api:order-list")

    def test_authenticated_manager_can_view_orders(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.manager_token.key)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_authenticated_chef_can_view_orders(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.chef_token.key)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_unauthenticated_user_cannot_view_orders(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class ProductsViewTests(APITestCase):
    def setUp(self):
        # Create a manager user and obtain its token
        self.manager_user = User.objects.create_user(
            email="manager@test.com",
            password="testpassword",
            user_role=User.UserRolesChoices.RESTAURANT_MANAGER,
        )
        self.manager_token, _ = Token.objects.get_or_create(user=self.manager_user)
        # Create some sample products
        self.product1 = Product.objects.create(
            name="Burger",
            description="Burger",
            price="900.00",
            image="http://127.0.0.1:6070/media/products-images/product_one.jpeg",
        )
        self.product2 = Product.objects.create(
            name="Pizza",
            description="Pizza",
            price="1999.00",
            image="http://127.0.0.1:6070/media/products-images/product_two.jpeg",
        )
        self.product3 = Product.objects.create(
            name="Rice",
            description="Rice",
            price="2300.00",
            image="http://127.0.0.1:6070/media/products-images/product_three.jpeg",
        )
        self.url = reverse("api:products")

    def test_get_products(self):
        self.client.credentials(HTTP_AUTHORIZATION=f"Token {self.manager_token.key}")
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            len(response.data), 3
        )  # Ensure correct number of products returned
        # Ensure response data matches the sample response format
        self.assertIn("name", response.data[0])
        self.assertIn("description", response.data[0])
        self.assertIn("price", response.data[0])
        self.assertIn("image", response.data[0])


class MenusViewTests(APITestCase):
    def setUp(self):
        # Create a manager user and obtain its token
        self.manager_user = User.objects.create_user(
            email="manager@test.com",
            password="testpassword",
            user_role=User.UserRolesChoices.RESTAURANT_MANAGER,
        )
        self.manager_token, _ = Token.objects.get_or_create(user=self.manager_user)
        # Create some sample menus
        self.menu1 = Menu.objects.create(name="Breakfast")
        self.menu2 = Menu.objects.create(name="Lunch")
        self.menu3 = Menu.objects.create(name="Dinner")
        self.url = reverse("api:menus")

    def test_get_menus(self):
        self.client.credentials(HTTP_AUTHORIZATION=f"Token {self.manager_token.key}")
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            len(response.data), 3
        )  # Ensure correct number of menus returned
        # Ensure response data matches the sample response format
        self.assertIn("name", response.data[0])

    def test_create_menu(self):
        self.client.credentials(HTTP_AUTHORIZATION=f"Token {self.manager_token.key}")
        data = {"name": "Dessert"}
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # Ensure response data matches the posted data
        self.assertEqual(response.data["name"], "Dessert")


class PlaceOrderViewTests(APITestCase):
    def setUp(self):
        # Create a user and obtain its token
        self.user = User.objects.create_user(
            email="testuser@test.com", password="testpassword"
        )
        self.token, _ = Token.objects.get_or_create(user=self.user)
        # Create some sample products
        self.product1 = Product.objects.create(
            name="Product 1",
            price="10.00",
            image="http://127.0.0.1:6070/media/products-images/product_one.jpeg",
        )
        self.product2 = Product.objects.create(
            name="Product 2",
            price="20.00",
            image="http://127.0.0.1:6070/media/products-images/product_two.jpeg",
        )
        self.url = reverse("api:place-order")

    def test_place_order_success(self):
        self.client.credentials(HTTP_AUTHORIZATION=f"Token {self.token.key}")
        data = {
            "items": [
                {"product": self.product1.id, "quantity": 4},
                {"product": self.product2.id, "quantity": 3},
            ]
        }
        response = self.client.post(self.url, data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data["success"], True)
        self.assertEqual(response.data["message"], "Order placed successfully")
        # Ensure order and order lines are created in the database
        self.assertTrue(Order.objects.exists())
        self.assertEqual(OrderLine.objects.count(), 2)  # Two order lines created

    def test_place_order_invalid_product(self):
        self.client.credentials(HTTP_AUTHORIZATION=f"Token {self.token.key}")
        data = {
            "items": [
                {"product": 9999, "quantity": 4},  # Invalid product ID
                {"product": self.product2.id, "quantity": 3},
            ]
        }
        response = self.client.post(self.url, data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["message"], "Invalid product ids provided")
        # Ensure no order or order lines are created in the database
        self.assertFalse(Order.objects.exists())
        self.assertEqual(OrderLine.objects.count(), 0)
