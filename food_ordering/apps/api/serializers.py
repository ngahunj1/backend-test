import json
import logging
from rest_framework import serializers
from decimal import Decimal
from django.db import transaction
from apps.orders.models import Order, OrderLine
from apps.catalog.models import Menu, Product

logger = logging.getLogger(__name__)


class OrderLineSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderLine
        fields = ["product", "quantity", "item_price", "line_item_total"]


class OrderSerializer(serializers.ModelSerializer):
    lines = OrderLineSerializer(many=True, read_only=True)

    class Meta:
        model = Order
        fields = ["number", "total", "status", "payment_status", "lines"]


class OrderStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ["number", "status"]


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ["id", "name", "description", "price", "image"]


class MenuSerializer(serializers.ModelSerializer):
    products = ProductSerializer(many=True, read_only=True)

    class Meta:
        model = Menu
        fields = ["name", "products"]


class OrderItemSerializer(serializers.Serializer):
    product = serializers.CharField()
    quantity = serializers.IntegerField()


class PlaceOrderSerializer(serializers.Serializer):
    items = serializers.ListField(child=OrderItemSerializer())

    def create(self, validated_data):
        # print(validated_data, "VD")

        data_ = json.dumps(validated_data)
        data = json.loads(data_)

        order_items = data.get("items")

        product_ids = [product["product"] for product in order_items]

        if not self.all_products_exists(product_ids):
            error = {"message": "Invalid product ids provided", "success": False}
            raise serializers.ValidationError(error)

        return self.place_order(order_items)

    def all_products_exists(self, products_list: list) -> bool:
        existing_products_count = Product.objects.filter(id__in=products_list).count()

        if existing_products_count == len(products_list):
            return True

        return False

    def place_order(self, order_items: list) -> None:
        try:
            with transaction.atomic():
                new_order = Order.objects.create()
                order_lines = [
                    OrderLine(
                        product_id=line_item.get("product"),
                        quantity=line_item.get("quantity"),
                        order=new_order,
                        line_item_total=Decimal(
                            Product.objects.get(id=line_item.get("product")).price
                            * line_item.get("quantity")
                        ),
                        item_price=Product.objects.get(
                            id=line_item.get("product")
                        ).price,
                    )
                    for line_item in order_items
                ]
                OrderLine.objects.bulk_create(order_lines)

            return new_order

        except Exception as e:
            logger.warning(f"place_order error {e}")
            return None
