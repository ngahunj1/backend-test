import uuid

from django.db import models

from django.contrib.auth.models import AbstractUser
from django.conf import settings

from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _
from rest_framework.authtoken.models import Token

from .managers import UserManager


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class User(AbstractUser):
    class UserRolesChoices(models.TextChoices):
        CHEF = "Chef", _("Chef")
        RESTAURANT_MANAGER = "Restaurant Manager", _("Restaurant Manager")
        USER = "User", _("User")

    username = None
    email = models.EmailField(_("Email Address"), unique=True)
    phone_number = models.CharField(
        max_length=100,
        verbose_name="Phone Number",
        unique=True,
        blank=True,
        null=True,
    )
    uid = models.UUIDField(
        unique=True,
        editable=False,
        default=uuid.uuid4,
        verbose_name="Public identifier",
    )
    user_role = models.CharField(
        _("User Type"),
        max_length=70,
        choices=UserRolesChoices.choices,
        default=UserRolesChoices.USER,
    )

    USERNAME_FIELD = "email"
    EMAIL_FIELD = "email"
    REQUIRED_FIELDS = []

    objects = UserManager()

    class Meta:
        verbose_name = _("User")
        verbose_name_plural = _("Users")

    def __str__(self):
        return self.email

    @property
    def is_manager(self):
        return self.user_role == self.UserRolesChoices.RESTAURANT_MANAGER

    @property
    def is_chef(self):
        return self.user_role == self.UserRolesChoices.CHEF

    @property
    def is_user(self):
        return self.user_role == self.UserRolesChoices.USER
