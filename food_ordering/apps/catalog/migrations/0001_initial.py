# Generated by Django 4.2.10 on 2024-03-01 08:20

import apps.catalog.utils
import apps.common.models.fields.fields
from decimal import Decimal
from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('internal_id', models.UUIDField(default=uuid.uuid4, editable=False, unique=True)),
                ('created', apps.common.models.fields.fields.CreationDateTimeField(auto_now_add=True, db_index=True, verbose_name='created')),
                ('modified', apps.common.models.fields.fields.ModificationDateTimeField(auto_now=True, db_index=True, verbose_name='modified')),
                ('name', models.CharField(max_length=250)),
                ('description', models.TextField()),
                ('price', models.DecimalField(decimal_places=2, max_digits=12, validators=[django.core.validators.MinValueValidator(Decimal('0.0'))])),
                ('image', models.ImageField(upload_to=apps.catalog.utils.product_upload_image_path, validators=[django.core.validators.validate_image_file_extension], verbose_name='Default Product Image')),
                ('created_by', models.ForeignKey(blank=True, default=None, editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='created_by_%(app_label)s_%(class)s_set', to=settings.AUTH_USER_MODEL, verbose_name='created by')),
                ('modified_by', models.ForeignKey(blank=True, default=None, editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='modified_by_%(app_label)s_%(class)s_set', to=settings.AUTH_USER_MODEL, verbose_name='modified by')),
            ],
            options={
                'verbose_name': 'Food Item',
                'verbose_name_plural': 'Food Items',
                'ordering': ['-created'],
            },
        ),
        migrations.CreateModel(
            name='Menu',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('internal_id', models.UUIDField(default=uuid.uuid4, editable=False, unique=True)),
                ('created', apps.common.models.fields.fields.CreationDateTimeField(auto_now_add=True, db_index=True, verbose_name='created')),
                ('modified', apps.common.models.fields.fields.ModificationDateTimeField(auto_now=True, db_index=True, verbose_name='modified')),
                ('name', models.CharField(max_length=250, unique=True)),
                ('created_by', models.ForeignKey(blank=True, default=None, editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='created_by_%(app_label)s_%(class)s_set', to=settings.AUTH_USER_MODEL, verbose_name='created by')),
                ('modified_by', models.ForeignKey(blank=True, default=None, editable=False, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='modified_by_%(app_label)s_%(class)s_set', to=settings.AUTH_USER_MODEL, verbose_name='modified by')),
                ('products', models.ManyToManyField(related_name='menus', to='catalog.product')),
            ],
            options={
                'verbose_name': 'Menu',
                'verbose_name_plural': ' Menus',
                'ordering': ['-created'],
            },
        ),
    ]
