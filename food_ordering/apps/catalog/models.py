from django.db import models
from decimal import Decimal
from django.core.validators import MinValueValidator
from django.utils.translation import gettext_lazy as _
from django.core.validators import validate_image_file_extension
from apps.common.models import AuthTimeStampedModel

from .utils import product_upload_image_path


class Menu(AuthTimeStampedModel):
    name = models.CharField(max_length=250, unique=True)
    products = models.ManyToManyField("catalog.Product", related_name="menus")

    class Meta:
        ordering = ["-created"]
        verbose_name = _("Menu")
        verbose_name_plural = _(" Menus")

    def __str__(self):
        return self.name


class Product(AuthTimeStampedModel):
    """Represents a Food Item in a menu"""

    name = models.CharField(max_length=250)
    description = models.TextField()
    price = models.DecimalField(
        max_digits=12,
        decimal_places=2,
        validators=[MinValueValidator(Decimal("0.0"))],
    )

    image = models.ImageField(
        _("Default Product Image"),
        upload_to=product_upload_image_path,
        validators=[validate_image_file_extension],
    )

    class Meta:
        ordering = ["-created"]
        verbose_name = _("Food Item")
        verbose_name_plural = _("Food Items")

    def __str__(self):
        return self.name

    @property
    def get_title(self):
        return self.name
