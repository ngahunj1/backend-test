from django.contrib import admin

from .models import Product, Menu


admin.site.register(Menu)
admin.site.register(Product)
