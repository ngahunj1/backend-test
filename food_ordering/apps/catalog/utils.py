import os


def get_filename_ext(filepath):
    base_name = os.path.basename(filepath)
    name, ext = os.path.splitext(base_name)
    return name, ext


def product_upload_image_path(instance, filename):
    """
    Generate the upload path for an image belonging to a product.\n

    Arguments:
        instance {[instance]} -- [The product instance]
        filename {[string]} -- [The name of the file being uploaded]
    """
    name, ext = get_filename_ext(filename)
    file_name = f"{name}{ext}"

    return f"products-images/{file_name}"
