import logging
from django.db import models

from django.utils.translation import gettext_lazy as _
from apps.common.models import AuthTimeStampedModel
from django.utils.crypto import get_random_string
from django.db import transaction

logger = logging.getLogger(__name__)


class Order(AuthTimeStampedModel):
    class OrderStatusPipeline(models.TextChoices):
        PENDING = "Pending", _("Pending")
        IN_PROGRESS = "In Progress", _("In Progress")
        COMPLETED = "Completed", _("Completed")

    class OrderPaymentStatusChoices(models.TextChoices):
        FULLY_PAID = "Fully Paid", _("Fully Paid")
        NOT_PAID = "Not Paid", _("Not Paid")
        PARTIALLY_PAID = ("Partially Paid", _("Partially Paid"))
        FAILED = "Failed", _("Failed")

    number = models.CharField(
        _("Order number"),
        max_length=77,
        db_index=True,
        unique=True,
        blank=True,
        null=True,
    )

    total = models.DecimalField(
        _("Order Total"), default=0.00, max_digits=100, decimal_places=2
    )
    status = models.CharField(
        _("Status"),
        max_length=100,
        choices=OrderStatusPipeline.choices,
        default=OrderStatusPipeline.PENDING,
    )
    payment_status = models.CharField(
        _("Payment Status"),
        max_length=70,
        choices=OrderPaymentStatusChoices.choices,
        default=OrderPaymentStatusChoices.NOT_PAID,
    )

    class Meta:
        ordering = ["-created"]
        verbose_name = _("Order")
        verbose_name_plural = _("Orders")

    def save(self, *args, **kwargs):
        if not self.number:
            self.number = self.__class__.generate_order_id()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.get_number

    @classmethod
    def generate_order_id(cls, length=6):
        """
        Get a unique random generated key
        """
        while True:
            number = get_random_string(length=length, allowed_chars="0123456789")
            if not cls._default_manager.filter(number=number).exists():
                return number

    @property
    def get_number(self):
        return self.number

    @property
    def is_pending(self):
        return self.status == self.__class__.OrderStatusPipeline.PENDING

    @property
    def is_completed(self):
        return self.status == self.__class__.OrderStatusPipeline.COMPLETED

    @property
    def is_in_progress(self):
        return self.status == self.__class__.OrderStatusPipeline.IN_PROGRESS

    def get_available_statuses(self):
        available_statuses = []

        if self.is_pending:
            available_statuses.extend((self.__class__.OrderStatusPipeline.IN_PROGRESS,))

        elif self.is_in_progress:
            available_statuses.extend((self.__class__.OrderStatusPipeline.COMPLETED,))

        return available_statuses

    def set_status(self, new_status: str):
        """Update the order status

        Args:
            new_status: (str) The new order status which is one of `self.__class__.OrderStatusPipeline.values`
        """
        if new_status == self.status:
            return

        if new_status not in self.__class__.OrderStatusPipeline.values:
            raise Exception(
                _(
                    "'%(new_status)s' is not a valid status for order %(number)s"
                    " (current status: '%(status)s')"
                )
                % {
                    "new_status": new_status,
                    "number": self.number,
                    "status": self.status,
                }
            )

        try:
            with transaction.atomic():
                self.status = new_status
                self.save()

        except Exception as er:
            logger.warning("set_status error", error=er)

    set_status.alters_data = True


class OrderLine(AuthTimeStampedModel):
    order = models.ForeignKey(
        "orders.Order",
        on_delete=models.CASCADE,
        related_name="lines",
        verbose_name=_("Order"),
    )

    product = models.ForeignKey(
        "catalog.Product",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name=_("Product"),
    )
    quantity = models.PositiveIntegerField(_("Quantity"), default=1)
    item_price = models.DecimalField(default=0.00, max_digits=100, decimal_places=2)

    line_item_total = models.DecimalField(max_digits=10, decimal_places=2, blank=True)

    class Meta:
        # Enforce sorting in order of creation.
        ordering = ["pk"]
        verbose_name = _("Order Line")
        verbose_name_plural = _("Order Lines")

    def __str__(self):
        if self.product:
            title = self.product.get_title
        else:
            title = _("<missing product>")
        return _("Product '%(name)s', quantity '%(qty)s'") % {
            "name": title,
            "qty": self.quantity,
        }
