from .auth_timestamped import AuthTimeStampedModel  # noqa
from .timestamped import TimeStampedModel  # noqa

__all__ = ["TimeStampedModel", "AuthTimeStampedModel"]
