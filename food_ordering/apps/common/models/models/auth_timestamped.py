from __future__ import unicode_literals

from django.contrib.auth import get_user_model
from django.db import models

from .timestamped import TimeStampedModel


class AuthTimeStampedModel(TimeStampedModel):
    """
    An abstract base class model that provides "created_by", "modified_by", "created" and "modified" fields
    to track changes to model instances.
    """

    created_by = models.ForeignKey(
        get_user_model(),
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        default=None,
        editable=False,
        related_name="created_by_%(app_label)s_%(class)s_set",
        verbose_name="created by",
    )
    modified_by = models.ForeignKey(
        get_user_model(),
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        default=None,
        editable=False,
        related_name="modified_by_%(app_label)s_%(class)s_set",
        verbose_name="modified by",
    )

    class Meta:
        abstract = True
