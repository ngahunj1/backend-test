from __future__ import unicode_literals

import uuid

from django.db import models
from django.utils.translation import gettext_lazy as _


from apps.common.models.fields import (
    CreationDateTimeField,
    ModificationDateTimeField,
)


class TimeStampedModel(models.Model):
    """
    TimeStampedModel
    An abstract base class model that provides self-managed "created" and
    "modified" fields.
    """

    internal_id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    created = CreationDateTimeField(_("created"))
    modified = ModificationDateTimeField(_("modified"))

    def save(self, **kwargs):
        self.update_modified = kwargs.pop(
            "update_modified", getattr(self, "update_modified", True)
        )
        super().save(**kwargs)

    class Meta:
        get_latest_by = "modified"
        abstract = True
