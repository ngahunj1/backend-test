#Food_ordering

Food_ordering is a food ordering application using Django framework with role-based authentication. The application supports Restaurant Manager, User, and Chefs.


## Required Features

1. Restaurant Manager:Add products: Ability to add new food items to the menu with details such as name, description, price, and image for .
2. Restaurant Manager:Manage orders: View and manage incoming orders, update their status, and mark them as completed.
3. User:Place orders
3. User: Track order progress: View the status of placed orders (e.g., pending, in progress, completed).
4: Chef:Update order status: Mark orders as in progress and complete once food preparation is done (Only chef can mark an order as complete).



## Installation and Setup

## Create a virtual environment

```
python3 -m venv venv;
source venv/bin/activate
```
If you need to install virtualenv:
```
virtualenv venv
```

## Activate the virtual environment
Before you begin you will need to activate the corresponding environment
```
source venv/bin/activate
```

## clone the repo 
```
git clone https://gitlab.com/ngahunj1/backend-test.git
```

cd into the project


```
pip install -r requirements/dev.txt
```

## Running the application
First create a .env file in the project root that contains the following
```
SECRET_KEY=SECRET_KEY
DEBUG=1
ENV=DEV

```
After the configuration, you will first run the migrations
```
python manage.py makemigrations
python manage.py migrate
```

Create a super user
```
python manage.py createsuperuser
```

Run the local server

```
python manage.py runserver
```

To run tests; 
cd ito `food_ordering` then run
```
python manage.py test
```

## Access the api's
Navigate to `http://localhost:8000/api/v1/`

####  To access the API docs visit
http://127.0.0.1:8000/docs/


## Deployed API
Access the deployed api
Navigate to [live api](http://18.216.63.99/api/v1/)

To access the live docs visit [Live api docs](http://18.216.63.99/docs)

### The production credentials are 
Admin Email: test@user.com
Password: 12345

### Auth token that can be used  for production testing is;
`18a9593c12a02e76ee43af086d8b2a1bcbed9e83`